var User = require('../models/user');

exports.postSignup = function(req, res, next){
    User.findOne({username: req.body.username}, function(err,existinguser){
        if(existinguser){
            return res.send('already exist');
        }
        if(!existinguser){
            var user = new User({
                username : req.body.username,
                password : req.body.password
            });

            user.save(function(err){
                if(err){
                    return res.send(err);
                }
                return res.send('ok');
            })
        }

    })

};