var History = require('../models/history');

module.exports = function(io) {
    var history;

    io.on('connection', function (socket) {
        socket.on('commands', function(data){
            history = new History({
               username : data.user,
               command : data.cmd,
               date : new Date()
            });

            history.save(function(err) {
                if (err) {
                    console.error(err);
                }
            })
        })
    })

};