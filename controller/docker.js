var Docker = require('dockerode');
var docker = new Docker({socketPath: '/var/run/docker.sock'});
var config = require('./../models/config');

exports.startDockerAndGetConnectionString = function(cb) {
    docker.createContainer(
        {
            Image: config.dockerWrapperImage,
            Privileged: true
        },
        function (err, container) {
            if(err){
                cb(err,null);
            }
            container.start(function (err, data) {
                if (err) {
                    cb(err,null);
                }
                container.inspect(function (err, data) {
                    cb(null,'http://'+data.NetworkSettings.IPAddress+':'+config.dockerWrapperPort);
                });
            });
        })
};