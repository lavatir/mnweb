var mnweb = {};
var cmdMode;
var dockerSocket;
var serverSocket;

mnweb.getNetworkFor = function(param){
    switch (param){
        case 'topo':{
            cmdMode = 'topo';
            dockerSocket.emit('data', 'net\r');
            break;
        }
        case 'export':{
            cmdMode = 'export';
            dockerSocket.emit('data', 'net\r');
            break;
        }
        default:
            cmdMode = ''
    }
};

mnweb.run = function(options) {
    dockerSocket = io.connect(options.dockerInstance);
    serverSocket = io.connect();
    cmdMode = '';

    var term = new Terminal({
        cols: 80,
        rows: 24,
        useStyle: true,
        screenKeys: true,
        cursorBlink: false
    });

    serverSocket.on('connect', function () {
        var buff = '';
        term.on('data', function (data) {
            if(data.charCodeAt(0) == 127){
                buff=buff.slice(0,-1);
            }else {
                buff += data;
            }
            if (data === '\r') {
                buff = buff.replace(/\r/g,'');
                if (buff != '') {
                    serverSocket.emit('commands', {user: options.user, cmd: buff});
                    $('#history').append($('<li>').text(buff));
                    $('#history')[0].scrollTop = $('#history')[0].scrollHeight;
                    buff = '';
                }
            }
        });
    })

    dockerSocket.on('connect', function () {
        var buff;
        term.on('data', function (data) {
            dockerSocket.emit('data', data);
        });
        term.open(options.terminalDiv);
        term.write('Welcome to your Mininet Instance!\n');
        dockerSocket.on('data', function (data) {
            switch(cmdMode){
                case 'topo': {
                    buff += data;
                    if (buff.search(/mininet>/) != -1) {
                        var parsedNetwork = mnweb.parse(buff);
                        buff = '';
                        cmdMode = '';
                        mnweb.drawNetwork(parsedNetwork, options.myNetwork);
                    }
                    break;
                }
                case 'export':{
                    buff += data;
                    if (buff.search(/mininet>/) != -1) {
                        var parsedNetwork = mnweb.parse(buff);
                        buff = '';
                        cmdMode = '';
                        mnweb.exportNetwork(parsedNetwork);
                    }
                    break;
                }
                default:
                    term.write(data);
            }
        });
        dockerSocket.on('disconnect', function () {
            term.destroy();
        });
    });
}

mnweb.parse = function (input){
    var splitted = input.replace(/\r|\n|\r\n|\s/g, ',').split(',');
        //$('#net').append($('<li>').text(splitted))
    var nodes = [];
    var edges = [];

    var counter = 0;
    for (var i = 0; i < splitted.length; i++) {
        if (splitted[i].length < 4 && splitted[i].match(/[hsc]([0-9]){1,2}/)) {
            nodes.push({id: counter+1, label: splitted[i]});
                counter++;
            }
        }
    for (var i = 0; i < nodes.length; i++) {
        if (nodes[i].label.match(/^[hs](\d){1,2}/)) {
            for (var j = 0; j < splitted.length; j++) {
                var reString = '^' + nodes[i].label;
                var re = new RegExp(reString);
                if (splitted[j].search(/eth([0-9]){1,2}/) != -1 && splitted[j].match(re)) {
                    edges.push({from: nodes[i].id, to: splitted[j].split(':')[1].replace(/-eth([0-9]){1,2}(,){0,1}/, '')})
                }
            }
        }
    }

    for (var i = 0; i < edges.length; i++) {
        for (var j = 0; j < nodes.length; j++) {
            if (edges[i].to === nodes[j].label) {
                edges[i].to = nodes[j].id;
            }
        }
    }

    for(var i=0; i<edges.length; i++){
        for(var j=i+1; j<edges.length;j++){
            if(edges[i].to === edges[j].from && edges[i].from === edges[j].to){
                edges[j].to=0;
                edges[j].from=0;
            }
        }
    }

    //$('#net').append($('<li>').text(JSON.stringify(nodes) + ' --- ' + JSON.stringify(edges)));
    return [nodes, edges];
}


mnweb.drawNetwork = function (parsedNetwork, panel){
    var visNodes = new vis.DataSet(parsedNetwork[0]);
    var visEdges = new vis.DataSet(parsedNetwork[1]);

    var visData = {
        nodes: visNodes,
        edges: visEdges
    };

    var visOptions = {};

    var network = new vis.Network(panel,visData,visOptions);
};

mnweb.exportNetwork = function(parsedNetwork){
    var nodes = parsedNetwork[0];
    var edges = parsedNetwork[1];
    var topo ='';

    for(var i=0;i<nodes.length;i++){
        switch (nodes[i].label[0]){
            case 'h':{
                if(nodes[i].label != 'h1' && nodes[i].label != 'h2'){
                    topo += 'py net.addHost(\''+nodes[i].label +'\')\n';
                }
                break;
            }
            case 's':{
                if(nodes[i].label != 's1'){
                    topo += 'py net.addSwitch(\''+nodes[i].label +'\')\n';
                    topo += 'py '+nodes[i].label +'.start([c0])\n';
                }
                break;
            }
            default : break;
        }
    }

    var fromNode;
    var toNode;
    for(var i=0; i<edges.length;i++){
        if(edges[i].from != 0 && edges[i].to != 0){
            for(var j=0;j<nodes.length;j++){
                if(edges[i].from == nodes[j].id){
                    fromNode=nodes[j].label;
                }
                if(edges[i].to == nodes[j].id){
                    toNode=nodes[j].label;
                }
            }
            if(!(fromNode == 'h1' && toNode == 's1') && !(fromNode == 'h2' && toNode == 's1')){
                topo+='py net.addLink(\''+fromNode+'\',\''+ toNode+'\')\n';
            }
        }
    }
    var blob = new Blob([topo]);
    saveAs(blob,'topology_export.mn');
};

$(document).on('change','#inputFile',function (event) {
    if ('FileReader' in window) {
        var fileToLoad = event.target.files[0];

        if (fileToLoad) {
            var reader = new FileReader();
            reader.onload = function (fileLoadEvent) {
                var fileContent = fileLoadEvent.target.result;
                var splitted = fileContent.split('\n');
                for(var i = 0;i <splitted.length;i++){
                    dockerSocket.emit('data',splitted[i]+'\r')
                }
            }
            reader.readAsText(fileToLoad, 'UTF-8');
            $('#importModal').modal('toggle');
        }
    } else {
        alert('Your browser does not support HTML5 FileReader!')
    }
});
