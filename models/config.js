module.exports = {
    dockerWrapperImage : 'mnwrapper:v0.9',
    dockerWrapperPort : 8080,
    mongoDbUrl : 'mongodb://localhost/mnweb',
    expressSessionConfig :{
        secret : 'titkosKuuuulcs',
        resave: false,
        saveUninitialized : false
    }
}