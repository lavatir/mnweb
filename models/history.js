var mongoose = require('mongoose');

var HistorySchema = new mongoose.Schema({
    username: String,
    command: String,
    date : Date
});

module.exports = mongoose.model('MnCmdHistory', HistorySchema)