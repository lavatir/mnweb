#!/usr/bin/env node

var http = require('http');
var express = require('express');
var io = require('socket.io');
var pty = require('pty.js');
var terminal = require('term.js');
var fs = require ('fs');
var exec = require('child_process').exec;

var app = express();
var server = http.createServer(app);

server.listen(8080);

io = io.listen(server, {
    log: false
});

app.use(terminal.middleware());

var socket;
var term;
var buff = [];
var gotConnection=false;

exec('service openvswitch-switch start', function() {

    term = pty.fork('mn', [], {
        name: fs.existsSync('/usr/share/terminfo/x/xterm-256color') ? 'xterm-256color' : 'xterm',
        cols: 80,
        rows: 24,
        cwd: process.env.HOME
    });

    term.on('data', function(data) {
        return !socket ? buff.push(data) : socket.emit('data', data);
    });

    setTimeout(function(){
        if(!gotConnection){
            process.exit(1);
        }
    }, 5000);
    console.log('Created mininet instance with pty master/slave pair (master: %d, pid: %d)', term.fd, term.pid);
});


io.sockets.on('connection', function(sock) {
    socket = sock;
    gotConnection=true;

    socket.on('data', function(data) {
        term.write(data);
    });

    socket.on('disconnect', function() {
        process.exit(0);
    });

    while (buff.length) {
        socket.emit('data', buff.shift());
    };

});
