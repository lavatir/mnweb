var express = require('express');
var router = express.Router();
var docker = require('../controller/docker');
var History = require('../models/history');

router.get('/',
    function(req,res,next){
      if(req.isAuthenticated()){
        next();
      }else {
          res.redirect('/')
      }
    },
    function(req, res) {
        docker.startDockerAndGetConnectionString(function(err,connectString){
            if(err){
                res.render('error',{error : err});
            }
            History.find({username: req.user.username}, function(err, data){
                res.render('mymn', {
                    title: 'MNWeb for '+req.user.username,
                    dockerInstance: connectString,
                    user: req.user.username,
                    cmdHistory: data
                })
            }).sort({'date': 'desc'}).limit(100).sort({'date': 'asc'});
        })
    }
);

module.exports = router;
