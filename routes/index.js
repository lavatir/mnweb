var express = require('express');
var router = express.Router();
var passport = require('passport');
var userController = require('../controller/user.js')

require('../controller/auth').middleware(passport);

router.get('/', function(req, res) {
    res.render('index', { title: 'MNWeb'})
});

router.post('/login',
    passport.authenticate('local',{
        successRedirect: '/mymn',
        failureRedirect: '/'
    })
);

router.post('/logout', function(req, res){
    req.logOut();
    res.redirect('/');
});

router.post('/register',userController.postSignup);

module.exports = router;
